import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import Debug from 'debug';
import express from 'express';
import logger from 'morgan';
import path from 'path';
// import favicon from 'serve-favicon';

import index from './routes/index';
import auth from './routes/auth';
import user from './routes/user';
import account from './routes/account';
import admin from './routes/admin';
import postback from './routes/postback';

const socket_io = require('socket.io');
const app = express();
const exphbs  = require('express-handlebars');
const debug = Debug('earn-coffee:app');
const config = require('./config').default;

const io = socket_io();
app.io = io;

app.set('io', io);

require('./routes/socket')(io);

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/earn', {
    useMongoClient: true
}, (err) => {
    if (err) return console.log('Failed to connect to database');
    console.log('Connected to database')
});

app.set('view engine', 'hbs');
app.engine('hbs', exphbs());

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", config.origin);
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

app.use(cookieParser());

app.use('/resources', express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/auth', auth);
app.use('/user', user);
app.use('/account', account);
app.use('/admin', admin);
app.use('/postback', postback);

// catch 404 and forward to error handler
app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
/* eslint no-unused-vars: 0 */
app.use((err, req, res, next) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    // render the error page
    res.status(err.status || 500);
    res.json(err);
});

// Handle uncaughtException
process.on('uncaughtException', (err) => {
    debug('Caught exception: %j', err);
    process.exit(1);
});

export default app;
