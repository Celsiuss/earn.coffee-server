let config = {
    secret: 'secret',

    sendgridAPI: 'key',

	origin: 'localhost',

    walletApiHost: 'localhost:5000',
    walletApiCode: 'CODE',

    walletIdentifier: 'ID',
    walletPassword: 'password',

    offerdaddyKey: '',

    recaptchaSecret: '',

    gClientId: ''
};

export default config;
