const events = require('events');

let event = new events.EventEmitter();

let handler = function (userId) {
    console.log(userId)
};

event.on('points', handler);

export default event;
