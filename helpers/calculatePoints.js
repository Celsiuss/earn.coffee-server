const User = require('../models/user');
const Transaction = require('../models/transaction');

module.exports = function (userId, callback) {
    User.findById(userId, (err, user) => {
        if (err) return callback({code: 500, message: 'Database error'}, null);
        if (!user) return callback({code: 400, message: 'User does not exist'});

        Transaction.find({userId: user._id}, (err, transactions) => {
            if (err) return callback({code: 500, message: 'Database error'}, null);
            if (!transactions) return callback({code: 200, amount: 0});

            let amount = 0;
            let total = 0;

            for (let transaction of transactions) {
                amount = amount + transaction.amount;
                if (transaction.amount > 0) total = total + transaction.amount;
            }
            user.points = amount;
            user.totalPoints = total;
            user.save((err) => {
                if (err) console.log(err);
            });
            callback(null, amount, total);

            let event = require('../events/transaction').default;
            event.emit('points', userId, amount)

        });

    });

};
