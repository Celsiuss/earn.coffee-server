module.exports = function (email, name, token) {
    let msg = {
        to: email,
        from: 'noreply@earn.coffee',
        templateId: 'd2510922-10bd-493f-b0af-14a37589e6cf',
        substitutions: {
            name: name,
            link: `https://earn.coffee/account/verify/${token}`
        }
    };

    sgMail.send(msg)
        .catch(error => {
            console.error(error)
        });
};
