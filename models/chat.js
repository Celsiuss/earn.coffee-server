const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let chatSchema = new Schema({
    id: {type: String, required: true},
    name: {type: String, required: true},
    avatar: {type: String, required: true},
    message: {type: String, required: true},
    time: {type: Date, required: true}
});

let Chat = mongoose.model('Chat', chatSchema);

module.exports = Chat;
