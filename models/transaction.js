const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let schema = new Schema({
    userId: {type: String, required: true},
    from: {type: String, required: true},
    amount: {type: Number, required: true},
    commission: {type: Number},
    to: {type: String},
    tx: {type: String},
    campaignName: {type: String},
    campaignId: {type: String},
    ip: {type: String},
    leadId: {type: String},
    country: {type: String},
    date: {type: Number, required: true}
});

let Transaction = mongoose.model('Transaction', schema);

module.exports = Transaction;
