const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let userSchema = new Schema({
    username: {type: String, required: true},
    email: {type: String, required: true, unique: true},
    password: {type: String},
    created: {type: Date, default: Date.now()},
    points: {type: Number, default: 0},
    totalPoints: {type: Number, default: 0},
    disabled: {type: Boolean, default: false},
    muted: {type: Boolean, default: false},
    verified: {type: Boolean, default: false},
    gravatar: {type: String, required: true},
    bio: {type: String},
    role: {type: String, default: 'user', required: true},
    type: {type: String, default: 'email', required: true},
    referrer: {type: String, default: 'none', required: true},
    referrals: {type: String, default: 0, required: true},
    referralEarnings: {type: String, default: 0, required: true}
});

let User = mongoose.model('User', userSchema);

module.exports = User;
