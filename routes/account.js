import express from 'express';

const router = express.Router();
const jwt = require('jsonwebtoken');
const config = require('../config').default;
const User = require('../models/user');
const Transaction = require('../models/transaction');
const MyWallet = require('blockchain.info/MyWallet');
const exchange = require('blockchain.info/exchange');
const WAValidator = require('wallet-address-validator');
const calculatePoints = require('../helpers/calculatePoints');

let wallet = new MyWallet(config.walletIdentifier, config.walletPassword, {apiHost: config.walletApiHost});

router.get('/balance', (req, res, next) => {
    res.send(wallet.getBalance())
});

router.use('', (req, res, next) => {
    if (req.method === 'OPTIONS') return res.send('');

    let auth = req.get('authorization');
    if (!auth) return res.status(401).send({
        message: 'Not authenticated'
    });
    jwt.verify(auth, config.secret, (err, decoded) => {
        if (!decoded || err) return status(401).send({message: 'Invalid token'});
        User.findById(decoded.user, (err, user) => {
            if (err) return res.status(500).send({message: 'Database error'});
            if (!user) return res.status(401).send({message: 'User does not exist'});

            req.user = user;
            next();
        });
    });

});

router.post('/updateName', (req, res, next) => {
    if (!req.body.name) return res.status(401).send({message: 'Name must be provided'});

    if (req.body.name.length > 15 || req.body.name.length < 4) return res.status(401).send({
        message: 'Name must be equal or over 4 characters and equal or under 15 characters.'
    });

    req.user.username = req.body.name;

    req.user.save((err, updatedUser) => {
        if (err) return res.status(500).send({message: 'Database error'});
        res.status(201).send({message: 'Name successfully updated!'});
    });

});

router.get('/getUserData', (req, res, next) => {
    res.status(200).send({message: 'success', obj: req.user})
});

router.get('/transactions', (req, res, next) => {
    Transaction.find({userId: req.user._id})
        .limit(20)
        .sort('-date')
        .exec( (err, transactions) => {
            if (err) return res.status(500).send({message: 'Database error', error: err});

            try {

                let newTransactions = [];

                for (let transaction of transactions) {
                    newTransactions.push({
                        _id: transaction._id,
                        userId: transaction.userId,
                        from: transaction.from,
                        amount: transaction.amount,
                        campaign: transaction.campaignName,
                        date: transaction.date,
                        to: transaction.to,
                        tx: transaction.tx
                    });

                }

                res.status(200).send({message: 'success', obj: newTransactions});
            } catch (e) {
                console.log(e);
            }
        });
});

router.post('/withdraw', (req, res, next) => {
    let address = req.body.address;
    let amount = (parseInt(req.body.amount) - 200) / 1000;

    if (!(address && amount)) return res.status(400).send({message: 'Address and amount required.'});
    if (amount < 1) return res.status(400).send({message: 'Amount must be over 1000 after fee.'});
    if (!WAValidator.validate(address, 'BTC')) return res.status(400).send({message: 'Invalid Bitcoin address.'});

    let counter = 3;

    let bitcoin = 0;
    let satoshi = 0;

    let points = 0;
    let balance = 0;

    let user = req.user;

    exchange.toBTC(amount * 100000000, 'USD').then(response => {
        bitcoin = parseFloat(response);
        satoshi = bitcoin * 100000000;
        nextCounter();
    });

    exchange.toBTC(user.points * 100000000, 'USD').then(response => {
        points = parseFloat(response) * 100000000;
        nextCounter();
    });

    wallet.getBalance().then(response => {
        balance = response.balance;
        nextCounter();
    });

    function send() {
        if (!user) return res.status(401).send({message: 'User does not exist'});
        if (points - satoshi < 0) return res.status(400).send({message: 'Not enough funds'});
        if (satoshi > balance) return res.status(500).send({message: 'Sorry, we don\'t have enough BTC in stock. Try again later'});

        wallet.send(address, satoshi, {from: 0})
            .then(payment => {
                if (!payment.success) return res.status(500).send({message: 'Something went wrong'});
                let transaction = new Transaction({
                    userId: req.user._id,
                    from: 'Withdrawal',
                    amount: parseInt(req.body.amount) * -1,
                    to: address,
                    tx: payment.txid,
                    date: Date.now()
                });
                transaction.save(err => {
                    if (err) return res.status(500).send({message: 'This is bad, the transaction went through but it was not saved to the DB so you didn\'t lose any points. Free money for you!'});
                    res.status(200).send({message: payment.amounts[0] / 100 + ' bits sent to address ' + payment.to[0]});

                    calculatePoints(req.user._id, () => {});
                });
            });
    }

    function nextCounter() {
        counter--;
        if (counter <= 0) {
            send();
        }
    }

});

export default router;
