import express from 'express';

const router = express.Router();
const jwt = require('jsonwebtoken');
const config = require('../config').default;
const User = require('../models/user');
const Transaction = require('../models/transaction');
const uid = require('uid-safe');
const calculatePoints = require('../helpers/calculatePoints');
const MyWallet = require('blockchain.info/MyWallet');

let wallet = new MyWallet(config.walletIdentifier, config.walletPassword, {apiHost: config.walletApiHost, apiCode: config.walletApiCode});

router.use('', (req, res, next) => {
    if (req.method === 'OPTIONS') return res.send('');

    let auth = req.get('authorization');
    if (!auth) return res.status(401).send({
        message: 'Not authenticated'
    });
    jwt.verify(auth, config.secret, (err, decoded) => {
        if (!decoded || err) return res.status(401).send({message: 'Invalid token'});

        User.findById(decoded.user, (err, user) => {
            if (user.role !== 'admin') return res.status(401).send({message: 'no'});
            req.user = user;
            next();
        });
    });

});

router.get('/stats', (req, res, next) => {
    User.count({}, (err, userCount) => {
        Transaction.count({}, (err, transactionCount) => {
            res.status(200).send({
                message: 'success',
                obj: {
                    users: userCount,
                    transactions: transactionCount
                }
            })
        });
    });
});

router.post('/userInfo', (req, res, next) => {
    let userId = req.body.userId;
    User.findById(userId, (err, user) => {
        if (!user) return res.status(404).send({message: 'The user does not exist'});

        res.status(200).send({message: 'Success', obj: user});

    });
});

router.post('/updateUserInfo', (req, res, next) => {
    let userId = req.body.userId;
    let username = req.body.username;
    let email = req.body.email;
    let role = req.body.role;
    let verified = req.body.verified;

    User.findById(userId, (err, user) => {
        if (!user) return res.status(404).send({message: 'The user does not exist'});

        user.username = username;
        user.email = email;
        if (role) user.role = role;
        if (verified) user.verified = verified;

        user.save((err) => {
            if (err) return res.status(500).send({message: 'Some error occured', error: err});
            res.status(200).send({message: 'Success'});
        });

    });

});

router.post('/muteUser', (req, res, next) => {
    let userId = req.body.userId;
    User.findById(userId, (err, user) => {
        if (!user) return res.status(404).send({message: 'The user does not exist'});
        user.muted = !user.muted;
        user.save((err) => {
            if (err) return res.status(500).send({message: 'Database error'});
            res.status(200).send({message: user.username + (user.muted ? ' muted' : ' unmuted')});
        });
    })
});

router.post('/disableUser', (req, res, next) => {
    let userId = req.body.userId;
    User.findById(userId, (err, user) => {
        if (!user) return res.status(404).send({message: 'The user does not exist'});
        user.disabled = !user.disabled;
        user.save((err) => {
            if (err) return res.status(500).send({message: 'Database error'});
            res.status(200).send({message: user.username + (user.disabled ? ' disabled' : ' reenabled')});
        });
    })
});

router.get('/blockchainAccount', (req, res, next) => {
    wallet.listAccounts().then(response => {
        res.send(response[0])
    });
});

router.post('/newTransaction', (req, res, next) => {
    let userId = req.body.userId;
    let amount = req.body.amount;
    let from = req.body.from;
    if (!(userId && amount && from)) return res.status(400).send({message: 'All fields required'});

    User.findById(userId, (err, user) => {
        if (err) return res.status(500).send({message: 'Database error'});
        if (!user) return res.status(400).send({message: 'User does not exist'});
        uid(18, (err, uid) => {
            if (err) return res.status(500).send({message: 'What? Try again'});
            let object = {
                userId: user._id,
                from: from,
                amount: amount,
                date: Date.now(),
                reference: uid
            };
            let transaction = new Transaction(object);
            transaction.save((err) => {
                if (err) return res.status(500).send({message: 'Database error', err: err});
                res.status(200).send({message: 'Success'});

                calculatePoints(user._id, (err, amount, total) => {
                    user.points = amount;
                    user.totalPoints = total;
                    user.save((err) => {
                        if (err) console.log(err);
                    });
                });

            });
        });

    });

});

export default router;
