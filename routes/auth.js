import express from 'express';

import sendVerificationEmail from '../helpers/sendVerificationEmail';

const bcrypt = require('bcrypt');
const router = express.Router();
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const md5 = require('md5');
const config = require('../config').default;
const rp = require('request-promise');
const GoogleAuth = require('google-auth-library');

let auth = new GoogleAuth;
let client = new auth.OAuth2(config.gClientId, '', '');

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(config.sendgridAPI);

let secret = config.secret;

router.post('/signup', (req, res) => {
    if (req.body.name && req.body.email && req.body.password && req.body.referrer) {
        if (!(req.body.name.length >= 4 && req.body.password.length >= 6)) return res.status(400).send({err: 'Invalid field length'});
    } else return res.status(400).send({err: 'All fields not supplied'});

    bcrypt.hash(req.body.password, 10, (err, hash) => {
        if (err) return console.error(err);
        let user = new User({
            username: req.body.name,
            email: req.body.email,
            password: hash,
            gravatar: `https://www.gravatar.com/avatar/${md5(req.body.email)}?r=pg&d=mm`,
            referrer: req.query.referrer
        });

        user.save((err) => {
            if (err) {
                if (err.code === 11000) {
                    return res.status(401).send({
                        message: 'Email already exists'
                    });
                } else {
                    return res.status(500).send({
                        message: 'An error occurred',
                        err: err
                    });
                }
            }
            res.status(201).send({
                message: 'User created'
            });

            jwt.sign({id: user._id, type: 'emailVerification'}, secret, (err, token) => {
                if (err) return res.status(500).send({message: 'shit'});

                sendVerificationEmail(user.email, user.username, token);

            });
        });
        if (req.body.referrer !== 'none') {
            User.findById(req.body.referrer, (err, user) => {
                if (err || !user) return;
                user.referrals++;
                user.save();
            });
        }

    })
});

router.post('/signin', (req, res, next) => {
    if (!(req.body.email || req.body.password)) return res.status(400).send({
        message: 'Email and/or password not supplied'
    });

    let options = {
        method: 'POST',
        uri: 'https://www.google.com/recaptcha/api/siteverify',
        form: {
            secret: config.recaptchaSecret,
            response: req.body.captcha,
            remoteip: req.headers['x-real-ip'] || req.connection.remoteAddress
        }
    };
    rp(options)
        .then(body => {
            console.log(body);
            if (JSON.parse(body).success !== true) return res.status(401).send({
                message: 'CAPTCHA failed'
            });


            User.findOne({email: req.body.email}, (err, user) => {
                if (err) console.log(err);
                if (user.type === 'google') return res.status(401).send({message: 'Please log in with Google'});
                if (!user) {
                    return res.status(401).send({
                        message: 'Invalid email or password'
                    });
                }
                bcrypt.compare(req.body.password, user.password, (err, valid) => {
                    if (err) return res.status(500).send({
                        message: 'Unknown error occurred, please try again'
                    });
                    if (!valid) return res.status(401).send({
                        message: 'Invalid email or password'
                    });
                    if (user.disabled) return res.status(401).send({
                        message: 'User is banned'
                    });
                    console.log(user);
                    let payload = {
                        user: user._id
                    };
                    jwt.sign(payload, secret, (err, token) => {
                        if (err) return res.status(500).send({
                            message: 'JWT error'
                        });

                        res.status(200).send({
                            message: 'User and passwords match',
                            obj: payload,
                            token: token
                        });

                    });
                });
            });
        })
        .catch(err => {
            res.status(500).send({
                message: 'Error, please try again'
            });
        });
});

router.post('/g-signin', (req, res, next) => {
    let token = req.body.token;
    if  (!token) return res.status(401).send({message: 'Token needed'});

    client.verifyIdToken(token, config.gClientId, (err, login) => {
        if (err) return res.send({message: 'Error', err: err});

        User.find({email: login.getPayload().email}, (err, user) => {
            user = user[0];
            if (err) return res.status(500).send({message: 'Database Error', err: err});
            if (user) {if (user.type !== 'google') return res.status(400).send({message: 'A user with this email already exist. Log in with email and password.'});}

            if (!user) {
                let user = new User({
                    username: login.getPayload().name,
                    email: login.getPayload().email,
                    verified: true,
                    gravatar: login.getPayload().picture,
                    type: 'google',
                    referrer: req.body.referrer
                });
                user.save(err => {
                    if (err) return res.status(500).send({message: 'Database error', err: err});
                    let payload = {
                        user: user._id,
                        type: 'google'
                    };
                    jwt.sign(payload, secret, (err, token) => {
                        res.status(201).send({
                            message: 'User created',
                            token: token
                        });
                    });
                });
                if (req.body.referrer !== 'none') {
                    User.findById(req.body.referrer, (err, user) => {
                        if (err || !user) return;
                        user.referrals = Number(user.referrals) + 1;
                        user.save();
                    });
                }
            } else {
                let payload = {
                    user: user._id,
                    type: 'google'
                };
                jwt.sign(payload, secret, (err, token) => {
                    res.status(200).send({
                        message: 'Logged in',
                        token: token
                    });
                });
            }

        });
    });
});

router.get('/verify', (req, res, next) =>{
    let authToken = req.get('authorization');
    jwt.verify(authToken, secret, (err, decoded) => {
        if (err) {
            if (err.name === 'TokenExpiredError') return res.status(500).send({
                err: err
            });
            return res.status(500).send({
                err: 'JWT error'
            });
        }
        User.findOne({_id: decoded.user}, (err, user) => {
            if (err) return res.status(500).send({
                message: 'Database error'
            });
            if (!user) return res.status(401).send({
                message: 'User does not exist'
            });
            if (user.disabled) return res.status(401).send({
                message: 'User is banned'
            });
            let payload = user.toObject();
            jwt.sign(payload, secret, (err, token) => {
                if (err) return res.status(500).send({message: 'No clue how this happened'});
                res.status(200).send({
                    message: 'success',
                    token: token,
                    user: payload
                });
            });

        });

    })
});

router.get('/verifyEmail', (req, res, next) => {
    let token = req.query.t;
    if (!token) return res.status(401).send({
        message: 'No token'
    });
    jwt.verify(token, secret, (err, decoded) => {
        if (err) return res.status(401).send({message: 'Invalid token'});
        if (decoded.type === 'emailVerification') return res.status(401).send({message: 'Invalid token'});
        User.findById(decoded.id, (err, user) => {
            if (err) return res.status(500).send({message: 'Database error'});
            if (!user) return res.status(401).send({message: 'User does not exist'});
            if (user.verified) return res.status(400).send({message: 'Email already verified, carry on!'});

            user.verified = true;

            user.save((err, updatedUser) => {
                if (err) return res.status(500).send({message: 'Database error'});
                res.status(201).send({message: 'Verified email ' + user.email});
            });
        })
    })
});

router.post('/requestPasswordReset', (req, res, next) => {
    let email = req.body.email;
    if (!email) return res.status(400).send({
        message: 'Email required'
    });
    User.findOne({email: email}, (err, user) => {
        if (err) return res.status(500).send({
            message: 'Database error'
        });
        if (!user) return res.status(400).send({
            message: 'Email does not exist'
        });
        jwt.sign({id: user._id, hash: user.password}, config.secret, (err, token) => {
            if (err) return res.status(500).send({
                message: 'An error occurred'
            });
            res.status(200).send({
                message: 'Password reset email sent!'
            });
            let mail = `
                <img src="https://i.imgur.com/hSERWnu.png" width="150" \>
                <h2>Password Reset</h2>
                <p>Hi <b>${user.username}</b></p>
                <p>You have requested a password reset for your account. Please use the link below to reset your password.</p>
                <a href="https://earn.coffee/account/reset/${token}" target="_blank">https://earn.coffee/account/reset/${token}</a>
                <p>If you did not perform this request, simply ignore this email.</p>
                `;
            smtpTransport.sendMail({
                from: '"Earn.coffee" <noreply@earn.coffee>',
                to: `"${user.username}" <${user.email}>`,
                subject: 'Earn.coffee Password Reset',
                forceEmbeddedImages: true,
                html: mail,
                generateTextFromHTML: true
            }, (err, info) => {
                if (err) return console.log(err);
            });
        })
    })
});

router.post('/resetPassword', (req, res, next) => {
    let password = req.body.password;
    let token = req.body.token;
    if (!password) return res.status(400).send({
        message: 'Password required'
    });
    if (!token) return res.status(400).send({
        message: 'Token required'
    });
    if (password.length < 6) return res.status(400).send({
        message: 'Password must 6 characters or more in length'
    });
    if (password.length > 100) return res.status(400).send({
        message: 'Why would you even use a password that long?'
    });
    jwt.verify(token, secret, (err, decoded) => {
        if (err) return res.status(400).send({
            message: 'Invalid token'
        });
        bcrypt.hash(password, 10, (err, hash) => {
            if (err) return res.status(500).send({
                message: 'Internal error'
            });
            User.findById(decoded.id, (err, user) => {
                if (err) return res.status(500).send({
                    message: 'Database error'
                });
                if (!user) return res.status(401).send({
                    message: 'User does not exist'
                });
                if (decoded.hash !== user.password) return res.status(401).send({
                    message: 'Password already reset'
                });
                user.password = hash;
                user.save((err, updatedUser) => {
                    if (err) return res.status(500).send({
                        message: 'Database error'
                    });
                    res.status(201).send({
                        message: 'Password updated'
                    });
                });
            });
        });
    });
});

export default router;
