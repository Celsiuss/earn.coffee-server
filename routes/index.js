import express from 'express';
import config from "../config";

const router = express.Router();
const Chat = require('../models/chat');
const nodemailer = require('nodemailer');

router.get('/', (req, res) => {
    res.render('index');
});

router.get('/test', (req, res) => {
    res.send({
        message: 'hi'
    })
});

router.get('/getMessages', (req, res) => {
    Chat.find({}).sort('-time').limit(20).exec(
        (err, messages) => {
            if (err) return res.status(500).send({
                message: 'db error'
            });
            res.status(200).send({
                message: 'ok',
                obj: messages
            });
        }
    )
});

let smtpTransport = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: config.emailUser,
        pass: config.emailPassword
    }
});

router.get('/email', (req, res, next) => {

    smtpTransport.sendMail({
        from: 'noreply@earn.coffee',
        to: 'danielsen2000@gmail.com',
        subject: 'test',
        text: 'hello',
        html: '<b>hello</b>'
    }, (err, info) => {
        if (err) return console.log(err);
        res.send(info);
    })

});

export default router;
