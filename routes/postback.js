import express from 'express';
import config from "../config";

const router = express.Router();
const Transaction = require('../models/transaction');
const User = require('../models/user');
const calculatePoints = require('../helpers/calculatePoints');
const md5 = require('md5');

router.get('/adworkmedia', (req, res, next) => {
    let ip = req.headers['x-real-ip'] ||
        req.connection.remoteAddress;

    if (ip === '::1' ||
        ip === '192.0.0.1' ||
        ip === '67.227.230.75' ||
        ip === '67.227.230.76' ||
        ip === '2607:fad0:3704:2::' ||
        ip === '2607:fad0:3704:2::1' ||
        ip === '2607:fad0:3704:2::2' ||
        ip === '2607:fad0:3704:2::3') {

        let subId = req.query.sid;
        let campaign = req.query.campaign_name;
        let payout = req.query.vc_value;

        if (req.query.status === 2) payout = Math.abs(payout) * -1;

        let transaction = new Transaction({
            userId: subId,
            from: 'AdWorkMedia',
            amount: payout,
            commission: req.query.commission,
            campaignName: campaign,
            campaignid: req.query.campaign_id,
            reversal_reason: req.query.reversal_reason,
            status: req.query.status,
            ip: req.query.ip,
            leadId: req.query.leadID,
            country: req.query.country,
            date: Date.now()
        });

        transaction.save(err => {
            if (err) return res.send({message: 'Database error', err: err});
            res.status(200).send('OK');

            calculatePoints(subId, () => {});
            giveReferralPoints(payout, subId);
        });
    } else {
        res.status(403).send('NO');
    }
});

router.get('/kiwiwall', (req, res, next) => {
    let ip = req.headers['x-real-ip'] ||
        req.connection.remoteAddress;

    if (ip === '::1' ||
        ip === '192.0.0.1' ||
        ip === '34.193.235.172') {

        let subId = req.query.sub_id;
        let campaign = req.query.offer_name;
        let payout = req.query.amount;

        let transaction = new Transaction({
            userId: subId,
            from: 'Kiwi Wall',
            amount: payout,
            commission: req.query.amount + req.query.gross,
            campaignName: campaign,
            campaignid: req.query.campaign_id,
            status: req.query.status,
            ip: req.query.ip_address,
            leadId: req.query.offer_id,
            country: '',
            date: Date.now()
        });

        transaction.save(err => {
            if (err) return res.send({message: 'Database error', err: err});
            res.status(200).send('1');

            calculatePoints(subId, () => {});
            giveReferralPoints(payout, subId);
        });
    } else {
        res.status(403).send('NO');
    }

});

router.get('/offerdaddy', (req, res, next) => {

    let subId = req.query.userid;
    let campaign = req.query.offer_name;
    let payout = req.query.amount;

    let transaction = new Transaction({
        userId: subId,
        from: 'Offer Daddy',
        amount: payout,
        campaignName: campaign,
        status: req.query.status,
        leadId: req.query.offer_id,
        country: '',
        date: Date.now()
    });

    if (md5(req.query.transaction_id + '/' + req.query.offer_id + '/' + config.offerdaddyKey) === req.query.signature) {
        transaction.save(err => {
            if (err) return res.send({message: 'Database error', err: err});
            res.status(200).send('1');

            calculatePoints(subId, () => {});
            giveReferralPoints(payout, subId);
        });
    } else {
        res.status(403).send('NO');
    }

});

router.get('/adgatemedia', (req, res, next) => {
    let ip = req.headers['x-real-ip'] ||
        req.connection.remoteAddress;

    if (ip === '::1' ||
        ip === '192.0.0.1' ||
        ip === '104.130.7.162') {

        let subId = req.query.s1;
        let campaign = req.query.offer_name;
        let payout = req.query.points;

        if (req.query.status === 0) payout = payout * -1;

        let transaction = new Transaction({
            userId: subId,
            from: 'AdGate Media',
            commission: req.query.payout,
            amount: payout,
            campaignName: campaign,
            campaignid: req.query.offer_id,
            reversal_reason: req.query.reversal_reason,
            status: req.query.status,
            ip: req.query.session_ip,
            leadId: req.query.affiliate_id,
            date: Date.now()
        });

        transaction.save(err => {
            if (err) return res.send({message: 'Database error', err: err});
            res.status(200).send('OK');

            calculatePoints(subId, () => {});
            giveReferralPoints(payout, subId);
        });
    } else {
        res.status(403).send('NO');
    }
});

router.get('/adscendmedia', (req, res, next) => {
    let ip = req.headers['x-real-ip'] ||
        req.connection.remoteAddress;

    if (ip === '::1' ||
        ip === '192.0.0.1' ||
        ip === '54.204.57.82' ||
        ip === '204.232.224.18' ||
        ip === '204.232.224.19' ||
        ip === '104.130.46.116' ||
        ip === '104.130.60.109' ||
        ip === '104.239.224.178' ||
        ip === '104.130.60.108') {

        let subId = req.query.sub;
        let campaign = req.query.offer_name;
        let payout = req.query.payout;

        if (req.query.status === 2) payout = Math.abs(payout) * -1;
        if (req.query.status === 3) return res.status(200).send('1');

        let transaction = new Transaction({
            userId: subId,
            from: 'AdscendMedia',
            amount: payout,
            commission: req.query.commission,
            campaignName: campaign,
            campaignid: req.query.offer_id,
            status: req.query.status,
            ip: req.query.ip,
            date: Date.now()
        });

        transaction.save(err => {
            if (err) return res.send({message: 'Database error', err: err});
            res.status(200).send('1');

            calculatePoints(subId, () => {});
            giveReferralPoints(payout, subId);
        });
    } else {
        res.status(403).send('NO');
    }
});

function giveReferralPoints(points, userId) {
    points = points * 0.05;
    User.findById(userId, (err, user) => {
        if (err || !user) return;
        if (user.referrer === 'none') return;
        let transaction = new Transaction({
            userId: user.referrer,
            from: 'Referral',
            amount: points,
            date: Date.now()
        });
        transaction.save(err => {
            calculatePoints(user.referrer, () => {});
            User.findById(user.referrer, (err, user) => {
                if (err || !user) return;
                user.referralEarnings = Number(user.referralEarnings + points);
                user.save(err => {if (err) console.error(err)});
            });

        });
    })
}

export default router;
