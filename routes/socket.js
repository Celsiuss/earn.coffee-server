import express from 'express';
const router = express.Router();
const jwt = require('jsonwebtoken');
const Chat = require('../models/chat');
const User = require('../models/user');
const config = require('../config').default;

let secret = config.secret;

let event = require('../events/transaction').default;

module.exports = function (io) {

    io.on('connection', (socket) => {
        // console.log(socket);

        socket.on('disconnect', () => {
            // console.log('closed connection')
        });

        event.on('points', (userId, points) => {
            io.emit('points', {id: userId, points: points});
        });

        socket.on('message', (message) => {
            if (message.message.length > 50) return;
            jwt.verify(message.token, secret, (err, decoded) => {
                if (err) return;
                User.findById(decoded.user, (err, user) => {
                    if (err) return console.error(err);
                    if (!user || user.muted) return;

                    let object = {
                        message: message.message,
                        name: user.username,
                        id: user._id,
                        avatar: user.gravatar,
                        time: Date.now()
                    };
                    io.emit('message', object);

                    let chat = new Chat(object);
                    chat.save(
                        err => {
                            if (err) console.log('db error\n' + err)
                        }
                    );

                });
            });
        })

    });

    return router;
};
