import express from 'express';

const router = express.Router();
const User = require('../models/user');
const jwt = require('jsonwebtoken');

router.get('/:id', (req, res, next) => {
    User.findOne({_id: req.params.id}, (err, user) => {
        if (!user) return res.status(404).send({
            message: 'User does not exist'
        });
        user.password = null;
        res.status(200).send({
            obj: user
        })
    })
});

export default router;
